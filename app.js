const express = require('express');
const app = express();

app.set('view engine', 'ejs');

app.use('/static', express.static('static'));
app.use('/node_modules', express.static('node_modules'));

const whatsAppLink = 'https://api.whatsapp.com/send?phone=77012222789&text=%D0%97%D0%B4%D1%80%D0%B0%D0%B2%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5,%20%D0%BD%D1%83%D0%B6%D0%BD%D0%B0%20%D0%B2%D0%B0%D1%88%D0%B0%20%D0%BF%D1%80%D0%BE%D1%84%D0%B5%D1%81%D1%81%D0%B8%D0%BE%D0%BD%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F%20%D0%BF%D0%BE%D0%BC%D0%BE%D1%89%D1%8C.'

app.get('/', function (req, res) {
    res.render(__dirname + '/views/index', {whatsAppLink: whatsAppLink});
});

app.get('/contacts', function (req, res) {
    res.render(__dirname + '/views/contacts', {whatsAppLink: whatsAppLink});
});

app.get('/equipment', function (req, res) {
    res.render(__dirname + '/views/equipment', {whatsAppLink: whatsAppLink});
});

app.listen(3000, () => {
    console.log('app is working')
});